# Pager_decoder

Demodulate a POCSAG signal using basic signal processing.

# Python interface

`main.py` was desigend as a diagnostic tool to develope and test algorithms which can be implemented down the line for a standalone decoder. The current basic implementation dose not use the error correction bits/parity bits to check the intagrity of the recieved data bits.


# Todo

- [x] Basic implementation based on recording.
- [ ] Error correction
- [ ] GNU radio based script
- [ ] real-time data logging and decording
- [ ] dedicated controller based on radio chip

