"""
Author : Hasith Perera (hasith@fos.cmb.ac.lk)
"""

from sqlite3 import DataError
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import wavfile

from scipy import signal
import math

import re


def get_cnt(data):
    old = 0;
    sig_old = 0;
    cnt = 0;
    
    bits = []
    sig_val = []
    
    for i in range(0,len(data)):
        current_sig = data[i]
        
        if sig_old != current_sig:
            bits.append(cnt)
            sig_val.append(sig_old)
            cnt = 0
            
        else:
            cnt = cnt + 1 
        sig_old = current_sig
        
    out = (np.array(bits), np.array(sig_val))
    return out

def drop_small_bits(datay):
    old = 0;
    sig_old = 0;
    cnt = 0;
    
    bits = []
    sig_val = []
    
    
    for i in range(0,len(datay)):
        current_sig = datay[i]
        
        if sig_old != current_sig:
            if cnt<50 :
                datay[i-cnt:i+1]=0

            bits.append(cnt)
            sig_val.append(sig_old)
            cnt = 0     
        else:
            cnt = cnt + 1 
        sig_old = current_sig
        
    out = (np.array(bits), np.array(sig_val))
    return datay

def to_bin(bits,fs=48000,baud=512):
    binary =[]
    dt = 48000/512
    print("Baud: {}, counts:{}".format(baud,dt))
    for i in range(0,len(bits[0])):
        for j in range(0,int(np.round(bits[0][i]/dt))):
            
            if (bits[1][i]):
                binary.append(0)
            else:
                binary.append(1)
    return binary
    

def decode_POCSAG(str_in):
    """Decode POCSAG packet from a binary string"""

    pattern = re.compile("01111100110100100001010111011000")
    r = pattern.search(str_in)
    ascii=''
    msg = ''
    start_of_msg = 0

    print("FSC found :({0}, {1})".format(r.start(), r.end() - 1))
    i = r.start()
    while i<len(str_in)-32:
        print(str_in[i:i+32],end="")

        if str_in[i:i+32]== "01111100110100100001010111011000":
            print(" | FSC |")
        
        elif str_in[i:i+32] =="01111010100010011100000110010111":
            print(" | --- |")

            if start_of_msg==1:
                break

        elif str_in[i]=="1":
            print(" | MSG |")
            ascii = "{}{}".format(ascii,str_in[i+1:i+21])


        elif str_in[i]=="0":
            print(" | ADD |")
            start_of_msg = 1

            
        else:
            print()

        i = i + 32

    # pad ascii text
    for k in range(0,7- len(ascii)%7):
        ascii = "{}0".format(ascii)

    for i in range(0,len(ascii),7):
        dec = 0
        #print(ascii[i:i+7],end=" ")
        
        for j in range(0,7):
            #print(i+j)
            dec = dec + int(ascii[i+j])*2**(j)
        msg="{}{}".format(msg,chr(dec))            

    return msg




if __name__=='__main__':
    
    fs, data = wavfile.read('/home/hasith/Music/pager/pkt8.wav')
    

    b, a = signal.butter(3, 0.05)
    #zi = signal.lfilter_zi(b, a)
    #z, _ = signal.lfilter(b, a, data, zi=zi*data[0])

    y = signal.filtfilt(b, a, data)
    sig = y>0
    cleaned = drop_small_bits(sig)

    bits = get_cnt(cleaned)
    final_bin = to_bin(bits)
    

    plt.subplot(121)
    plt.plot(data)
    plt.plot(y,'.')

    plt.plot(sig*20000-10000)
    plt.plot(cleaned*20000-10000)
    plt.xlim([0,2000])
    
    
    plt.subplot(122)
    plt.hist(bits[0][:],np.arange(0,200,1))


    bin_str = str(np.array2string(np.array(final_bin),
                                separator="",
                                max_line_width=len(final_bin),
                                threshold=len(final_bin)+5,
                                
                                )[1:-2])
    #print(bin_str)
    msg = decode_POCSAG(bin_str)

    print(msg)

    #plt.show()
    
    
